<?php

declare(strict_types=1);

namespace SlyFoxCreative\ResponsiveImages\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use SlyFoxCreative\ResponsiveImages\ServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }
}
