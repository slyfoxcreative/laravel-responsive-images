<?php

declare(strict_types=1);

namespace SlyFoxCreative\ResponsiveImages\Tests;

use StevenBerg\ResponsibleImages\Image;
use StevenBerg\ResponsibleImages\ResponsiveImageable;

class TestImage implements ResponsiveImageable
{
    public function toResponsiveImage(): Image
    {
        return new Image('test.jpg');
    }
}
