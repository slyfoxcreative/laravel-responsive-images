<?php

declare(strict_types=1);

namespace SlyFoxCreative\ResponsiveImages\Tests;

use StevenBerg\ResponsibleImages\SizeRange;
use StevenBerg\ResponsibleImages\Urls\Cloudinary;
use StevenBerg\ResponsibleImages\Urls\Maker;
use StevenBerg\ResponsibleImages\Values\Gravity;
use StevenBerg\ResponsibleImages\Values\Shape;
use StevenBerg\ResponsibleImages\Values\Size;
use StevenBerg\ResponsibleImages\Values\Version;

use function SlyFoxCreative\ResponsiveImages\responsive_image_for;
use function SlyFoxCreative\ResponsiveImages\responsive_image_tag;

class HelpersTest extends TestCase
{
    protected function setUp(): void
    {
        putenv('CLOUDINARY_API_KEY=test_key');
        putenv('CLOUDINARY_API_SECRET=test_secret');
        putenv('CLOUDINARY_CLOUD_NAME=test_name');

        parent::setUp();

        Maker::registerDefaultMaker(new Cloudinary());
    }

    public function testResponsiveImageFor(): void
    {
        $image = responsive_image_for(
            'test.jpg',
            gravity: Gravity::Center,
            shape: Shape::Wide,
            version: Version::from('123'),
        );

        self::assertEquals(
            'https://res.cloudinary.com/test_name/image/upload/c_fill,f_auto,fl_advanced_resize,g_center,h_50,q_auto:best,w_100/v123/test.jpg',
            $image->source(Size::from(100)),
        );
    }

    public function testResponsiveImageForWithDefaultOptions(): void
    {
        $image = responsive_image_for('test.jpg');

        self::assertEquals(
            'https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg',
            $image->source(Size::from(100)),
        );
    }

    public function testResponsiveImageForWithInvalidGravity(): void
    {
        $this->expectException(\DomainException::class);

        responsive_image_for(
            'test.jpg',
            shape: Shape::Original,
            gravity: Gravity::Auto,
        );
    }

    public function testResponsiveImageTag(): void
    {
        $image = responsive_image_for('test.jpg');
        $range = SizeRange::from(100, 100, 100);

        self::assertEquals(
            "<img alt='' sizes='100vw' src='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg' srcset='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg 100w'>",
            responsive_image_tag($image, $range),
        );
    }

    public function testResponsiveImageTagWithAttributes(): void
    {
        $image = responsive_image_for('test.jpg');
        $range = SizeRange::from(100, 100, 100);

        self::assertEquals(
            "<img alt='Test' sizes='50vw' src='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg' srcset='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg 100w'>",
            responsive_image_tag($image, $range, ['alt' => 'Test', 'sizes' => '50vw']),
        );
    }

    public function testResponsiveImageTagWithToResponsiveImage(): void
    {
        $image = new TestImage();
        $range = SizeRange::from(100, 100, 100);

        self::assertEquals(
            "<img alt='' sizes='100vw' src='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg' srcset='https://res.cloudinary.com/test_name/image/upload/c_scale,f_auto,fl_advanced_resize,q_auto:best,w_100/test.jpg 100w'>",
            responsive_image_tag($image, $range),
        );
    }
}
