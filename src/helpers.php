<?php

declare(strict_types=1);

namespace SlyFoxCreative\ResponsiveImages;

use StevenBerg\ResponsibleImages\Image;
use StevenBerg\ResponsibleImages\ResponsiveImageable;
use StevenBerg\ResponsibleImages\SizeRange;
use StevenBerg\ResponsibleImages\Values\Gravity;
use StevenBerg\ResponsibleImages\Values\Shape;
use StevenBerg\ResponsibleImages\Values\Version;

function responsive_image_for(
    string $name,
    Shape $shape = Shape::Original,
    ?Gravity $gravity = null,
    ?Version $version = null,
): Image {
    if ($shape === Shape::Original && ! is_null($gravity)) {
        throw new \DomainException('Gravity specified for non-cropped image');
    }

    $imageOptions = [];

    if ($shape !== Shape::Original) {
        $imageOptions['gravity'] = $gravity ?? Gravity::Auto;
    }

    if (! is_null($version)) {
        $imageOptions['version'] = $version;
    }

    return Image::fromShape($shape, $name, $imageOptions);
}

/** @param array<string, string> $attributes */
function responsive_image_tag(
    ResponsiveImageable $image,
    SizeRange $range,
    array $attributes = [],
): string {
    return $image->toResponsiveImage()->tag($range, $range->last(), $attributes);
}
