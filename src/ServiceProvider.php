<?php

declare(strict_types=1);

namespace SlyFoxCreative\ResponsiveImages;

use Cloudinary\Configuration\Configuration;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use StevenBerg\ResponsibleImages\Urls\Cloudinary as CloudinaryMaker;
use StevenBerg\ResponsibleImages\Urls\Maker;

class ServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-responsive-images')
            ->hasConfigFile()
        ;
    }

    public function packageBooted(): void
    {
        Configuration::instance(['cloud' => config('responsive-images.cloudinary')]);
        Maker::registerDefaultMaker(new CloudinaryMaker());
    }
}
